// Exponent Operator

// Template Literals
let getCube = 10;
console.log(`The Cube of ${getCube} is ${getCube**3}`);
// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
let [housenum,avenue,state,zip] = address

console.log(`i live in ${housenum} ${avenue}, ${state} ${zip}.`)


// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
};
let {name,species,weight,measurement} = animal;

console.log(`${name} was a ${species}. He weighted at ${weight} with a measurement of ${measurement}.`);


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

const array1 = [1,2,3,4,5];

array1.forEach(element => console.log(element));

// Javascript Classes

class aso{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let aso1 = new aso(`Jabbar`,3,`Pug`);
let aso2 = new aso(`Snow`,1,`Husky`);
let aso3 = new aso("Kimpee",10,`350z`)
console.log(aso1);
console.log(aso2);
console.log(aso3);

